package com.example.betmanagerthree.ui.stats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.betmanagerthree.databinding.FragmentStatBinding
import com.example.betmanagerthree.util.SavedPrefs

class StatFragment : Fragment() {
    private lateinit var binding: FragmentStatBinding
    private lateinit var viewModel: StatViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStatBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this)[StatViewModel::class.java]
        viewModel.calculateStat(SavedPrefs.getBets(binding.root.context))

        viewModel.totalAmount.observe(viewLifecycleOwner) {binding.textStatTotalBets.text = it}
        viewModel.successPercentage.observe(viewLifecycleOwner) {binding.textStatSuccessPercent.text = it}
        viewModel.profit.observe(viewLifecycleOwner) {binding.textStatSuccess.text = it}
        viewModel.roi.observe(viewLifecycleOwner) {binding.textStatRoi.text = it}
        viewModel.roc.observe(viewLifecycleOwner) {binding.textStatRoc.text = it}
        viewModel.averageBet.observe(viewLifecycleOwner) {binding.textStatAverageBet.text = it}
        viewModel.averageCoefficient.observe(viewLifecycleOwner) {binding.textStatAverageCoefficient.text = it}
        return binding.root
    }

}