package com.example.betmanagerthree.ui.bets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.betmanagerthree.data.Bet

class MainViewModel : ViewModel() {
    private var moneyAmount =0

    private val mutableMoneyData = MutableLiveData(0)
    val moneyData : LiveData<Int> = mutableMoneyData

    fun setMoney(amount: Int) {
        moneyAmount = amount
        mutableMoneyData.postValue(amount)
    }

    fun changeMoney(amount : Int) {
        moneyAmount += amount
        mutableMoneyData.postValue(moneyAmount)
    }

    fun changeBetState(bet: Bet, newState: Boolean?) {
        if(bet.state != newState) {
            if(bet.state == true) moneyAmount -= (bet.amount * bet.coefficient).toInt()

            if(newState == true) moneyAmount += (bet.amount * bet.coefficient).toInt()

            mutableMoneyData.postValue(moneyAmount)
        }
    }

    fun deleteBet(bet: Bet) {
        when(bet.state) {
            true -> moneyAmount -= (bet.amount * bet.coefficient).toInt()
            else -> moneyAmount += bet.amount
        }
        mutableMoneyData.postValue(moneyAmount)
    }
}