package com.example.betmanagerthree.ui.stats

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.betmanagerthree.data.Bet

class StatViewModel : ViewModel() {

    private val mutableTotalAmount = MutableLiveData<String>()
    val totalAmount : LiveData<String> = mutableTotalAmount

    private val mutableSuccessPercentage = MutableLiveData<String>()
    val successPercentage : LiveData<String> = mutableSuccessPercentage

    private val mutableProfit = MutableLiveData<String>()
    val profit : LiveData<String> = mutableProfit

    private val mutableROI = MutableLiveData<String>()
    val roi : LiveData<String> = mutableROI

    private val mutableROC = MutableLiveData<String>()
    val roc : LiveData<String> = mutableROC

    private val mutableAverageBet = MutableLiveData<String>()
    val averageBet : LiveData<String> = mutableAverageBet

    private val mutableAverageCoefficient = MutableLiveData<String>()
    val averageCoefficient : LiveData<String> = mutableAverageCoefficient

    fun calculateStat(bets : List<Bet>) {
        calculateTotalAmount(bets)
        calculateSuccessPercentage(bets)
        calculateProfit(bets)
        calculateROI(bets)
        calculateROC(bets)
        calculateAverageBet(bets)
        calculateAverageCoefficient(bets)
    }

    private fun calculateTotalAmount(bets : List<Bet>) = mutableTotalAmount.postValue(bets.size.toString())

    private fun calculateSuccessPercentage(bets : List<Bet>) {
        var successful = 0

        for( bet in bets) if (bet.state == true) successful++

        val p = if(bets.isEmpty()) "100 %"
        else {
            "${100 / bets.size * successful} %"
        }
        mutableSuccessPercentage.postValue(p)
    }

    private fun calculateProfit(bets : List<Bet>) {
        var profit = 0
        for(bet in bets) {
            profit -= bet.amount
            if(bet.state == true) profit += (bet.amount * bet.coefficient).toInt()
        }
        mutableProfit.postValue("$profit")
    }

    private fun calculateROI(bets : List<Bet>) {
        var profit = 0
        var spend = 0
        for(bet in bets) {
            spend += bet.amount
            if(bet.state == true) profit += (bet.amount * bet.coefficient).toInt()
        }
        val roi = if(bets.isEmpty()) 0
        else profit / spend
        mutableROI.postValue("$roi")
    }

    private fun calculateROC(bets : List<Bet>) {
        var profit = 0
        var spend = 0
        for(bet in bets) {
            spend += bet.amount
            if(bet.state == true) profit += (bet.amount * bet.coefficient).toInt()
        }
        mutableROC.postValue("${profit - spend}")
    }

    private fun calculateAverageBet(bets : List<Bet>) {
        var spend = 0
        for(bet in bets) { spend += bet.amount }
        mutableAverageBet.postValue("${if(bets.isEmpty()) 0 else spend / bets.size}")
    }

    private fun calculateAverageCoefficient(bets : List<Bet>) {
        var coeffSumm = 0.0f
        for(bet in bets) { coeffSumm += bet.coefficient }
        mutableAverageCoefficient.postValue("${if(bets.isEmpty()) 0 else coeffSumm / bets.size}")
    }
}