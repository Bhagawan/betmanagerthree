package com.example.betmanagerthree.ui.bets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.betmanagerthree.R
import com.example.betmanagerthree.data.Bet
import com.example.betmanagerthree.databinding.AlertAddMoneyBinding
import com.example.betmanagerthree.databinding.AlertNewBetBinding
import com.example.betmanagerthree.databinding.FragmentMainBinding
import com.example.betmanagerthree.databinding.PopupStateBinding
import com.example.betmanagerthree.util.BetAdapter
import com.example.betmanagerthree.util.SavedPrefs

class MainFragment : Fragment() {
    private lateinit var binding : FragmentMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        binding.recyclerBets.layoutManager = LinearLayoutManager(binding.root.context)
        val adapter = BetAdapter()
        adapter.updateList(SavedPrefs.getBets(binding.root.context))
        adapter.setInterface(object : BetAdapter.BetAdapterInterface {
            override fun changeState(bet: Bet) {
                this@MainFragment.changeState(bet)
            }

            override fun deleteBet(bet: Bet) {
                viewModel.deleteBet(bet)
                SavedPrefs.deleteBet(binding.root.context, bet)
            }
        })
        binding.recyclerBets.adapter = adapter

        viewModel.moneyData.observe(viewLifecycleOwner) {
            binding.textTotal.text = it.toString()
            SavedPrefs.saveTotal(binding.root.context, it)
        }
        viewModel.setMoney(SavedPrefs.getTotal(requireContext()))

        binding.layoutTotal.setOnClickListener { addMoney() }
        binding.btnBetsAdd.setOnClickListener { addBet() }
        return binding.root
    }

    private fun addMoney() {
        context?.let { context ->
            val builder = AlertDialog.Builder(context)
            val binding = AlertAddMoneyBinding.inflate(LayoutInflater.from(context))
            builder.setView(binding.root)

            val dialog = builder.create()
            binding.btnAddMoneyCancel.setOnClickListener { dialog.dismiss() }
            binding.btnAddMoneyAdd.setOnClickListener {
                val amount = try {
                    binding.editTextAddMoney.text.toString().toInt()
                } catch (e: Exception) { 0 }
                val total = SavedPrefs.getTotal(context) + amount
                viewModel.setMoney(total)
                SavedPrefs.saveTotal(context, total)
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun addBet() {
        context?.let { context ->
            val builder = AlertDialog.Builder(context)
            val alertBinding = AlertNewBetBinding.inflate(LayoutInflater.from(context))
            builder.setView(alertBinding.root)

            val dialog = builder.create()
            alertBinding.btnAddBetCancel.setOnClickListener { dialog.dismiss() }
            alertBinding.btnAddBetAdd.setOnClickListener {
                try {
                    val amount = alertBinding.editTextAddBetAmount.text.toString().toInt()
                    val coefficient = alertBinding.editTextAddBetCoefficient.text.toString().toFloat()
                    val bet = Bet(amount, coefficient, null)
                    SavedPrefs.addBet(context, bet)
                    (binding.recyclerBets.adapter as BetAdapter).addItem(bet)
                    viewModel.changeMoney(-amount)
                    dialog.dismiss()
                } catch (e: Exception) {
                    Toast.makeText(context, getString(R.string.error_input), Toast.LENGTH_SHORT).show()
                }
            }
            dialog.show()
        }
    }

    private fun changeState(bet: Bet) {
        context?.let { context ->
            val builder = AlertDialog.Builder(context, R.style.CustomDialog)
            val alertBinding = PopupStateBinding.inflate(LayoutInflater.from(context))
            builder.setView(alertBinding.root)
            when(bet.state) {
                true -> {
                    alertBinding.btnPopupStateNegative.isSelected = false
                    alertBinding.btnPopupStateWaiting.isSelected = false
                    alertBinding.btnPopupStatePositive.isSelected = true
                }
                false -> {
                    alertBinding.btnPopupStateNegative.isSelected = true
                    alertBinding.btnPopupStateWaiting.isSelected = false
                    alertBinding.btnPopupStatePositive.isSelected = false
                }
                else -> {
                    alertBinding.btnPopupStateNegative.isSelected = false
                    alertBinding.btnPopupStateWaiting.isSelected = true
                    alertBinding.btnPopupStatePositive.isSelected = false
                }
            }

            val dialog = builder.create()
            alertBinding.btnPopupStateNegative.setOnClickListener {
                viewModel.changeBetState(bet, false)
                alertBinding.btnPopupStateNegative.isSelected = true
                alertBinding.btnPopupStateWaiting.isSelected = false
                alertBinding.btnPopupStatePositive.isSelected = false
                SavedPrefs.changeBetState(context, bet, false)
                (binding.recyclerBets.adapter as BetAdapter).updateState(bet, false)
                dialog.dismiss()
            }
            alertBinding.btnPopupStateWaiting.setOnClickListener {
                viewModel.changeBetState(bet, null)
                alertBinding.btnPopupStateNegative.isSelected = false
                alertBinding.btnPopupStateWaiting.isSelected = true
                alertBinding.btnPopupStatePositive.isSelected = false
                SavedPrefs.changeBetState(context, bet, null)
                (binding.recyclerBets.adapter as BetAdapter).updateState(bet, null)
                dialog.dismiss()
            }
            alertBinding.btnPopupStatePositive.setOnClickListener {
                viewModel.changeBetState(bet, true)
                alertBinding.btnPopupStateNegative.isSelected = false
                alertBinding.btnPopupStateWaiting.isSelected = false
                alertBinding.btnPopupStatePositive.isSelected = true
                SavedPrefs.changeBetState(context, bet, true)
                (binding.recyclerBets.adapter as BetAdapter).updateState(bet, true)
                dialog.dismiss()
            }
            dialog.show()
        }
    }

}