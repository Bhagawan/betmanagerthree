package com.example.betmanagerthree

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.betmanagerthree.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_fragment_activity_main)
        navView.setupWithNavController(navController)
        loadBackground()
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, it) }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
        }
        Picasso.get().load("http://195.201.125.8/BetManagerThree/back.png").into(target)
    }
}