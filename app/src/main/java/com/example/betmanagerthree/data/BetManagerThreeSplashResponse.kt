package com.example.betmanagerthree.data

import androidx.annotation.Keep

@Keep
data class BetManagerThreeSplashResponse(val url : String)