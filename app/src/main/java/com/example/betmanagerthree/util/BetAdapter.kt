package com.example.betmanagerthree.util

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.betmanagerthree.data.Bet
import com.example.betmanagerthree.databinding.ItemBetBinding

class BetAdapter : RecyclerView.Adapter<BetAdapter.ViewHolder>() {
    private var items = ArrayList<Bet>()
    private var adapterInterface: BetAdapterInterface? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBetBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItem(item: Bet) {
        items.add(item)
        notifyItemInserted(items.size)
    }

    fun updateList(bets: List<Bet>) {
        for (bet in bets) if(!items.contains(bet)) addItem(bet)
    }

    fun updateState(bet: Bet, newState: Boolean?) {
        if(items.contains(bet)) {
            val i = items.indexOf(bet)
            items[i].state = newState
            notifyItemChanged(i)
        }
    }

    fun setInterface(adapterInterface: BetAdapterInterface) {
        this.adapterInterface = adapterInterface
    }

    inner class ViewHolder(private val binding: ItemBetBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Bet) {
            binding.textBetAmount.text = item.amount.toString()
            binding.textBetCoefficient.text = item.coefficient.toString()
            setTint(item.state)
            binding.itemBetLayout.setOnClickListener { adapterInterface?.changeState(item) }
            binding.btnItemBetDelete.setOnClickListener {
                items.remove(item)
                notifyItemRemoved(adapterPosition)
                adapterInterface?.deleteBet(item)
            }
        }

        private fun setTint(state : Boolean?) {
            val color = when(state) {
                true -> Color.parseColor("#5B7155")
                false -> Color.parseColor("#755351")
                null -> Color.WHITE
            }
            binding.itemBetLayout.backgroundTintList = ColorStateList.valueOf(color)
        }
    }

    interface BetAdapterInterface {
        fun changeState(bet: Bet)
        fun deleteBet(bet: Bet)
    }
}