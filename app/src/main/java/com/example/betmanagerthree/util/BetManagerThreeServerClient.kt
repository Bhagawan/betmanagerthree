package com.example.betmanagerthree.util

import com.example.betmanagerthree.data.BetManagerThreeSplashResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BetManagerThreeServerClient {

    @FormUrlEncoded
    @POST("BetManagerThree/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<BetManagerThreeSplashResponse>

    companion object {
        fun create() : BetManagerThreeServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(BetManagerThreeServerClient::class.java)
        }
    }

}