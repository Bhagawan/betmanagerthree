package com.example.betmanagerthree.util

import android.content.Context
import com.example.betmanagerthree.data.Bet
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SavedPrefs {
    companion object {
        fun saveTotal(context: Context, amount: Int) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            shP.edit().putInt("total", amount).apply()
        }

        fun getTotal(context: Context) : Int = context.getSharedPreferences("AppData", Context.MODE_PRIVATE).getInt("total", 0)

        fun addBet(context: Context, bet: Bet) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val g = Gson()
            val oldBets = g.fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList<Bet>()
            var updating = true
            for(b in oldBets) {
                if(b == bet) {
                    updating = false
                    b.state = bet.state
                }
            }
            val newBets = if(updating) oldBets.plus(bet) else oldBets
            shP.edit().putString("bets", g.toJson(newBets)).apply()
        }

        fun getBets(context: Context) : List<Bet> {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            return Gson().fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList()
        }

        fun deleteBet(context: Context, bet : Bet) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val g = Gson()
            val oldBets = g.fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList<Bet>()
            shP.edit().putString("bets", g.toJson(oldBets.minus(bet))).apply()
        }

        fun changeBetState(context: Context, bet : Bet, newState: Boolean?) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val g = Gson()
            val oldBets = g.fromJson(shP.getString("bets", "[]"), object: TypeToken<List<Bet>>() {}.type)?: emptyList<Bet>()
            oldBets[oldBets.indexOf(bet)].state = newState
            shP.edit().putString("bets", g.toJson(oldBets)).apply()
        }
    }
}